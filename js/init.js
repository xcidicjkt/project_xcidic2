;(function () { // IIFE
  $(function() { // Shorthand Document ready function
    if (location.href.indexOf("https://") == -1) { 
      location.href = location.href.replace("http://", "https://"); 
    } 
    var trigger = $('.hamburger'),
        trigger1 = $('.back'),
        overlay1 = $('.overlay1'),
        isClosed = false,
        window$= $(window),
        wrapper1 = $('#wrapper1'),
        wrapper2 = $('#wrapper2'),
        footer3 = $('.footer3'),
        careerSidebar = $('#career-sidebar'),
        footerIpadH2 = $('.footer-ipad h2'),
        slide3Bg1 = $('#slide-3 .bg1'),
        slide3Bg2 = $('#slide-3 .bg2'),
        slide3Bg3 = $('#slide-3 .bg3'),
        slide3Bg4 = $('#slide-3 .bg4'),
        slide3Bg1Highlight = $('#slide-3 .bg1-highlight'),
        slide3Bg2Highlight = $('#slide-3 .bg2-highlight'),
        slide3Bg3HighLight = $('#slide-3 .bg3-highlight'),
        slide3Bg4HighLight = $('#slide-3 .bg4-highlight'),
        submitPortfolio = $('#submit-portfolio'),
        submitPortfolioMb = $('#submit-portfolio-mb'),
        inputEmail = $('input[name=email]'),
        inputEmail1 = $('input[name=email1]'),
        inputEnquiry = $('textarea[name=enquiry]'),
        inputEnquiry1 = $('textarea[name=enquiry1]'),
        contact_button = $('#contact_button'),
        inputJobCareer = $('input[name=jobCareer]'),
        careerModal = $('#careerModal'),
        careerMbFormBtnSubmit = $('#career-mb-form .btn[type="submit"]'),
        contact_button1 = $('#contact_button1'),
        slide2SubDetail1 = $('#slide-2 .sub-detail1'),
        slide2SubDetail2 = $('#slide-2 .sub-detail2'),
        slide2SubDetail3 = $('#slide-2 .sub-detail3'),
        slide2SubDetail4 = $('#slide-2 .sub-detail4'),
        slide2Sub = $('#slide-2 .sub'),
        slide4SubDetail1 = $('#slide-4 .sub-detail1'),
        slide4SubDetail2 = $('#slide-4 .sub-detail2'),
        slide4SubDetail3 = $('#slide-4 .sub-detail3'),
        slide4SubDetail4 = $('#slide-4 .sub-detail4'),
        slide4Sub = $('#slide-4 .sub'),
        slide6SubDetail1 = $('#slide-6 .sub-detail1'),
        slide6SubDetail2 = $('#slide-6 .sub-detail2'),
        slide6SubDetail3 = $('#slide-6 .sub-detail3'),
        slide6Sub = $('#slide-6 .sub'),
        slide6SubMbDetail1 = $('#slide-6 .sub-mb-detail1'),
        slide6SubMbDetail2 = $('#slide-6 .sub-mb-detail2'),
        slide6SubMbDetail3 = $('#slide-6 .sub-mb-detail3'),
        slide6SubMb = $('#slide-6 .sub-mb');

      // makes sure the whole site is loaded
     window$.on('load', function() {
       $("#spinner").fadeOut();
       $("#pre-loader").delay(1500).fadeOut("slow");
     });

     var TOTAL_SLIDES = 7;
     $('#fullpage').fullpage({
         css3 : true,
         anchors:['home', 'about', 'what', 'works', 'culture', 'testimonial', 'career', 'blog', 'contact'],
         autoScrolling: false,
         scrollbar : true,
         fitToSection: false,
         fitToSectionDelay: 0,
         verticalCentered: false,
         loopHorizontal: true,
         controlArrows: true,

         afterLoad: function(anchorLink, index){
             $('#slide-0 .body-content').addClass('show-content');
         },

         afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
             for (var i = 0; i < TOTAL_SLIDES; i++) {
               $('#slide-' + i).removeClass('rotate-left-min');
               $('#slide-' + i).removeClass('rotate-left-max');
               $('#slide-' + i).removeClass('rotate-right-min');
               $('#slide-' + i).removeClass('rotate-right-max');
             }
               $('#slide-' + slideIndex + ' .body-content').addClass('show-content');

               $('.nav__item.active').removeClass('active');
               $('.nav__item').eq(slideIndex).addClass('active');

               $title = ["", "About Us", "What We Do", "Our Works", "Our Culture", "Testimonial", "Career",  "Our Blog", "Contact Us"];
               $('.title h1').text($title[slideIndex]);

               if (slideIndex === 0) {
                 footer3.css('display', 'none');
                 footerIpadH2.html('We exist to make difference to people\'s life');
                 $('.footer2 h2').text('');
               } else if(slideIndex === 3) {
                 footer3.css('visibility', 'visible');
                 footer3.css('display', 'block');
                 footerIpadH2.text('');
               }else {
                 footerIpadH2.text('');
                 footer3.css('visibility', 'hidden');
               }
           },

           onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
             var leavingSlide = $(this);
             var enteringSlide = $('#slide-' + nextSlideIndex);

             //rotate right
             if (direction === 'right') {
               $(leavingSlide).addClass('rotate-left-min');
               $(enteringSlide).addClass('rotate-right-min');
             }

             // rotate left
             if (direction === 'left') {
               $(leavingSlide).addClass('rotate-right-max');
               $(enteringSlide).addClass('rotate-left-max');
             }

             $('#slide-' + slideIndex + ' .body-content').removeClass('show-content');
           }
     });

     $.material.init();

      trigger.click(function () {
        hamburger_cross();
      });
      overlay1.click(function () {
        hamburger_cross();
      });
      $('li .menu').click(function () {
        hamburger_cross();
      });

      function hamburger_cross() {

        if (isClosed == true) {
          overlay1.hide();
          trigger.removeClass('is-open');
          trigger.addClass('is-closed');
          isClosed = false;
        } else {
          overlay1.show();
          trigger.removeClass('is-closed');
          trigger.addClass('is-open');
          isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function () {
       wrapper1.toggleClass('toggled');
    });
    $('[data-toggle="offcanvas1"]').click(function () {
       wrapper2.toggleClass('toggled');
    });
    $('[data-toggle="career-sidebar"]').click(function () {
      careerSidebar.toggleClass('toggled');
    });

    if(window$.width() >= 800) {

      slide3Bg1.hover(
        function() {
          slide3Bg1Highlight.css("background-image", "url(img/03h-1.png)");
        }, function(){
          slide3Bg1Highlight.css("background-image", "none");
       }
      );
      slide3Bg2.hover(
        function() {
          slide3Bg2Highlight.css("background-image", "url(img/03h-4.png)");
        }, function(){
          slide3Bg2Highlight.css("background-image", "none");
       }
      );
      slide3Bg3.hover(
        function() {
          slide3Bg3HighLight.css("background-image", "url(img/03h-2.png)");
        }, function(){
          slide3Bg3HighLight.css("background-image", "none");
       }
      );
      slide3Bg4.hover(
        function() {
          slide3Bg4HighLight.css("background-image", "url(img/03h-3.png)");
        }, function(){
          slide3Bg4HighLight.css("background-image", "none");
       }
      );
    }

    validate();
    $('#email_contact, #enquiry').keyup(validate);
    validate1();
    $('#email1, #enquiry1').keyup(validate1);


    $('form[data-toggle="validator"]').validator({
      custom: {
        filesize: function ($el) {
          var maxKilobytes = $el.data('filesize') * 1024000;
          if($el[0].files[0] != null) {
            if ($el[0].files[0].size > maxKilobytes) {
              submitPortfolio.prop("disabled", true);
              submitPortfolioMb.prop("disabled", true);
              return "File must be smaller than " + $el.data('filesize') + " MB."
            } else {
              submitPortfolio.prop("disabled", false);
              submitPortfolioMb.prop("disabled", false);
            }
           }
        },

        //custom file type validation
        filetype: function ($el) {
          var acceptableTypes = $el.prop('accept').split(",");
          if($el[0].files[0]) {
            var fileType = $el[0].files[0].type;

            if (!$.inArray(fileType, acceptableTypes)) {
              return "Invalid file type"
            }
          }
        }
      }
    });

    $('form[name=desktop-form]').submit(function(event) {
       var service_id = 'gmail';
       var template_id = 'xcidic_template';
       var template_params = {
         name: inputEmail.val(),
         message: inputEnquiry.val()
       };

       emailjs.send(service_id,template_id,template_params)
       .then(function(response) {
          swal({
           title: "Success!",
           text: "Your message was sent successfully. Thanks.",
           type: "success",
           allowOutsideClick: true
         });
          inputEmail.val("");
          inputEnquiry.val("");
          contact_button.button('reset');
       }, function(err) {
          contact_button.button('reset');
          swal({
            title: "Error!",
            text: "Failed to send your message. Please try again.",
            type: "error",
            allowOutsideClick: true
           });
       });
       event.preventDefault();
    });

    contact_button.on('click', function() {
        var $this = $(this);
        $this.button('loading');
    });

    $('.apply-career1').on('click', function() {
       inputJobCareer.val($(".apply-career1").data("job"));
       $.fn.fullpage.setKeyboardScrolling(false);
    });

    $('.apply-career2').on('click', function() {
        inputJobCareer.val($(".apply-career2").data("job"));
    });

    $('.apply-career3').on('click', function() {
        inputJobCareer.val($(".apply-career3").data("job"));
    });

    $('.apply-mb-career1').on('click', function() {
        $('input[name=jobCareerMb]').val($(this).data("job"));
    });

    $('.apply-mb-career2').on('click', function() {
        $('input[name=jobCareerMb]').val($(this).data("job"));
    });

    $('.apply-mb-career3').on('click', function() {
        $('input[name=jobCareerMb]').val($(this).data("job"));
    });

   careerModal.on('hidden.bs.modal', function (e) {
     $.fn.fullpage.setKeyboardScrolling(true);
   });

    var reader = new FileReader();

    $('form[name=career-form]').validator().on('submit', function (e) {
     if (e.isDefaultPrevented()) {

     } else {
        e.preventDefault();

          reader.onload = function () {
            var request = $.ajax({
                url: restApi('api/careers'),
                type: "post",
                data: {
                  fullName: $('input[name=fullNameCareer]').val(),
                  emailAddress: $('input[name=emailAddressCareer]').val(),
                  phoneNumber: $('input[name=phoneNumberCareer]').val(),
                  resume: reader.result,
                  portfolio: $('input[name=portfolioCareer]').val(),
                  jobType: inputJobCareer.val()
                },
                beforeSend: function () {
                  swal({ title: 'Please wait', animation: false, allowOutsideClick: false, allowEscapeKey: false, allowEnterKey: false });
                  swal.showLoading();
               }
            });
            request.done(function (){
                 swal({
                  title: "Success!",
                  text: "Your Application Has Been Submitted Successfully.",
                  type: "success",
                  allowOutsideClick: true
                });
                 $("#career-form")[0].reset();
                 careerModal.modal('toggle');
                 $('#career-form .btn[type="submit"]').button('reset');
            });
            request.fail(function (){
                swal({
                 title: "Error!",
                 text: "Failed to send your message. Please try again.",
                 type: "error",
                 allowOutsideClick: true
                });
            });
          };

          reader.readAsDataURL(document.getElementById('resumeCareer').files[0]);
     }
   });

    $('form[name=career-mb-form]').validator().on('submit', function (e) {
     if (e.isDefaultPrevented()) {

     } else {
        e.preventDefault();

        careerMbFormBtnSubmit.button('loading');
          setTimeout(function() {
             $this.button('reset');
         }, 50000);

        reader.onload = function () {
          var request = $.ajax({
              url: restApi('api/careers'),
              type: "post",
              data: {
                fullName: $('input[name=fullNameCareerMb]').val(),
                emailAddress: $('input[name=emailCareerMb]').val(),
                phoneNumber: $('input[name=phoneCareerMb]').val(),
                resume: reader.result,
                portfolio: $('input[name=portfolioCareerMb]').val(),
                jobType: $('input[name=jobCareerMb]').val()
              }
          });
          request.done(function (){
              swal({
                title: "Success!",
                text: "Your Application Has Been Submitted Successfully.",
                type: "success",
                allowOutsideClick: true
              });
               $("#career-mb-form")[0].reset();
               careerSidebar.toggleClass('toggled');
               careerMbFormBtnSubmit.button('reset');
          });
          request.fail(function (){
              swal({
               title: "Error!",
               text: "Failed to send your application. Please try again.",
               type: "error",
               allowOutsideClick: true
              });
          });
        }

        reader.readAsDataURL(document.getElementById('resumeCareerMb').files[0]);
     }
   });

    $('form[name=mobile-form]').submit(function(event) {

       var service_id = 'gmail';
       var template_id = 'xcidic_template';
       var template_params = {
         name: inputEmail1.val(),
         message: inputEnquiry1.val()
       };

       emailjs.send(service_id,template_id,template_params)
       .then(function(response) {
          contact_button1.button('reset');
          swal({
           title: "Success!",
           text: "Your message was sent successfully. Thanks.",
           type: "success",
           allowOutsideClick: true
         });
          inputEmail1.val("");
          inputEnquiry1.val("");
          wrapper2.toggleClass('toggled');
       }, function(err) {
          contact_button1.button('reset');
           swal({
            title: "Error!",
            text: "Failed to send your message. Please try again.",
            type: "error",
            allowOutsideClick: true
           });
       });
       event.preventDefault();
    });

    contact_button1.on('click', function() {
      var $this = $(this);
      $this.button('loading');
    });

    $( "#slide-1 .about-button" ).click(function() {
      $( "#slide-1 .half-content" ).toggle();
    });

    var testimonial_index = 0;
    var testimonials = [
      {
        name: "JOHN SEE TOH",
        position: "CHAIRMAN",
        company: "Runninghour Co-operative",
        image: "img/john.jpg",
        testimonial: "<img class='pick' src='img/picktwoopen.png'>&nbsp; We'd like to thank Xcidic for doing such a wonderful job in revamping our website. We absolutely love the artistic presentation and more importantly in taking the time to understand our organisation in order to help us reach out to our target audience. &nbsp;<img class='pick' src='img/picktwoclose.png'>"
      },
      {
        name: "STANLEY SOH",
        position: "M.C.",
        company: "for Weddings and Events",
        image: "img/stanley.jpg",
        testimonial: "<img class='pick' src='img/picktwoopen.png'>&nbsp; I would like to thank Xcidic for creating a professional and chic website for me! The feedback from my clients about the website was positive! I absolutely love the initiatives and ideas of the Xcidic Team! I will definitely recommend their services to everyone! &nbsp;<img class='pick' src='img/picktwoclose.png'>"
      },
      {
        name: "SKY LEE",
        position: "FOUNDER",
        company: "SB Rope Access Specialist",
        image: "img/skylee.jpg",
        testimonial: "<img class='pick' src='img/picktwoopen.png'>&nbsp; Xcidic is a very helpful and responsible web development company. Especially during the UAT phase, all issues logged was promptly and accurately addressed. They provided many genuine and helpful advices which enabled us to streamline our web development. &nbsp;<img class='pick' src='img/picktwoclose.png'>"
      }
    ];

    function indexChange() {

      if(testimonial_index == 0) {

        $( "#slide-5 .testimonial-button.up" ).hide();
        $( "#slide-5 .testimonial-mb-button.up" ).hide();
        $( "#slide-5 .testimonial-button.down" ).show();
        $( "#slide-5 .testimonial-mb-button.down" ).show();

      } else if(testimonial_index == 2) {

        $( "#slide-5 .testimonial-button.down" ).hide();
        $( "#slide-5 .testimonial-mb-button.down" ).hide();
        $( "#slide-5 .testimonial-button.up" ).show();
        $( "#slide-5 .testimonial-mb-button.up" ).show();

      } else {

        $( "#slide-5 .testimonial-button.up" ).show();
        $( "#slide-5 .testimonial-mb-button.up" ).show();
        $( "#slide-5 .testimonial-button.down" ).show();
        $( "#slide-5 .testimonial-mb-button.down" ).show();

      }
    }

    indexChange();

    $( "#slide-5 .testimonial-button.up" ).click(function() {
      testimonial_index--;

      $( "#slide-5 .testimonial-img img" ).attr("src", testimonials[testimonial_index].image);
      $('#slide-5 .testimonial-name h3').text(testimonials[testimonial_index].name);
      $('#slide-5 .testimonial-name h4').text(testimonials[testimonial_index].position);
      $('#slide-5 .testimonial-name p').text(testimonials[testimonial_index].company);
      $('#slide-5 .testimonial-content p').html(testimonials[testimonial_index].testimonial);

      indexChange();
    });

    $( "#slide-5 .testimonial-button.down" ).click(function() {
      testimonial_index++;

      $( "#slide-5 .testimonial-img img" ).attr("src", testimonials[testimonial_index].image);
      $('#slide-5 .testimonial-name h3').text(testimonials[testimonial_index].name);
      $('#slide-5 .testimonial-name h4').text(testimonials[testimonial_index].position);
      $('#slide-5 .testimonial-name p').text(testimonials[testimonial_index].company);
      $('#slide-5 .testimonial-content p').html(testimonials[testimonial_index].testimonial);

      indexChange();
    });

    $( "#slide-5 .testimonial-mb-button.up" ).click(function() {
      testimonial_index--;

      $( "#slide-5 .testimonial-mb-img img" ).attr("src", testimonials[testimonial_index].image);
      $('#slide-5 .testimonial-mb-name h3').text(testimonials[testimonial_index].name);
      $('#slide-5 .testimonial-mb-name h4').text(testimonials[testimonial_index].position);
      $('#slide-5 .testimonial-mb-name p').text(testimonials[testimonial_index].company);
      $('#slide-5 .testimonial-mb-content p').html(testimonials[testimonial_index].testimonial);

      indexChange();
    });

    $( "#slide-5 .testimonial-mb-button.down" ).click(function() {
      testimonial_index++;

      $( "#slide-5 .testimonial-mb-img img" ).attr("src", testimonials[testimonial_index].image);
      $('#slide-5 .testimonial-mb-name h3').text(testimonials[testimonial_index].name);
      $('#slide-5 .testimonial-mb-name h4').text(testimonials[testimonial_index].position);
      $('#slide-5 .testimonial-mb-name p').text(testimonials[testimonial_index].company);
      $('#slide-5 .testimonial-mb-content p').html(testimonials[testimonial_index].testimonial);

      indexChange();
    });

    $( "#slide-2 .open-what1" ).click(function() {
      slide2SubDetail1.show();
      slide2Sub.hide();
    });
    $( "#slide-2 .open-what2" ).click(function() {
      slide2SubDetail2.show();
      slide2Sub.hide();
    });
    $( "#slide-2 .open-what3" ).click(function() {
      slide2SubDetail3.show();
      slide2Sub.hide();
    });
    $( "#slide-2 .open-what4" ).click(function() {
      slide2SubDetail4.show();
      slide2Sub.hide();
    });
    $( "#slide-2 .close-what1" ).click(function() {
      slide2SubDetail1.hide();
      slide2Sub.show();
    });
    $( "#slide-2 .close-what2" ).click(function() {
      slide2SubDetail2.hide();
      slide2Sub.show();
    });
    $( "#slide-2 .close-what3" ).click(function() {
      slide2SubDetail3.hide();
      slide2Sub.show();
    });
    $( "#slide-2 .close-what4" ).click(function() {
      slide2SubDetail4.hide();
      slide2Sub.show();
    });

    $( "#slide-4 .open-culture1" ).click(function() {
      slide4SubDetail1.show();
      slide4Sub.hide();
    });
    $( "#slide-4 .open-culture2" ).click(function() {
      slide4SubDetail2.show();
      slide4Sub.hide();
    });
    $( "#slide-4 .open-culture3" ).click(function() {
      slide4SubDetail3.show();
      slide4Sub.hide();
    });
    $( "#slide-4 .open-culture4" ).click(function() {
      slide4SubDetail4.show();
      slide4Sub.hide();
    });
    $( "#slide-4 .close-culture1" ).click(function() {
      slide4SubDetail1.hide();
      slide4Sub.show();
    });
    $( "#slide-4 .close-culture2" ).click(function() {
      slide4SubDetail2.hide();
      slide4Sub.show();
    });
    $( "#slide-4 .close-culture3" ).click(function() {
      slide4SubDetail3.hide();
      slide4Sub.show();
    });
    $( "#slide-4 .close-culture4" ).click(function() {
      slide4SubDetail4.hide();
      slide4Sub.show();
    });

    $( "#slide-6 .open-career1" ).click(function() {
      slide6SubDetail1.show();
      slide6Sub.hide();
    });
    $( "#slide-6 .open-career2" ).click(function() {
      slide6SubDetail2.show();
      slide6Sub.hide();
    });
    $( "#slide-6 .open-career3" ).click(function() {
      slide6SubDetail3.show();
      slide6Sub.hide();
    });
    $( "#slide-6 .close-career1" ).click(function() {
      slide6SubDetail1.hide();
      slide6Sub.show();
    });
    $( "#slide-6 .close-career2" ).click(function() {
      slide6SubDetail2.hide();
      slide6Sub.show();
    });
    $( "#slide-6 .close-career3" ).click(function() {
      slide6SubDetail3.hide();
      slide6Sub.show();
    });

    $( "#slide-6 .open-mb-career1" ).click(function() {
      slide6SubMbDetail1.show();
      slide6SubMb.hide();
    });
    $( "#slide-6 .open-mb-career2" ).click(function() {
      slide6SubMbDetail2.show();
      slide6SubMb.hide();
    });
    $( "#slide-6 .open-mb-career3" ).click(function() {
      slide6SubMbDetail3.show();
      slide6SubMb.hide();
    });
    $( "#slide-6 .open-mb-career" ).click(function() {
      slide6SubMbDetail2.show();
      slide6SubMb.hide();
    });
    $( "#slide-6 .close-mb-career1" ).click(function() {
      slide6SubMbDetail1.hide();
      slide6SubMb.show();
    });
    $( "#slide-6 .close-mb-career2" ).click(function() {
      slide6SubMbDetail2.hide();
      slide6SubMb.show();
    });
    $( "#slide-6 .close-mb-career3" ).click(function() {
      slide6SubMbDetail3.hide();
      slide6SubMb.show();
    });
    function validate(){
       if (isEmail($('#email_contact').val()) && ($('#enquiry').val().length > 0)) {
          contact_button.prop("disabled", false);
       }
       else {
          contact_button.prop("disabled", true);
       }
    }

    // fetch blog articles
    $.getJSON(restApi('api/articles/published'), { limit: 2 }, function(result) {
      var itemsForDesktop = [];
      var itemsForMobile = [];
      $.each(result.data, function(key, val) {
        var articleForDesktop = ('<li class="article row list-unstyled" id="article_' + key + '">' +
                      '<div class="col-sm-3 article-img">' +
                        '<img class="img-responsive" src="' + val.imageThumbSquare + '">' +
                      '</div>' +
                      '<div class="col-sm-9 article-content">' +
                        '<a href="https://blog.xcidic.com/article/' + val.slug + '" target="_blank" rel="noopener noreferrer">' +
                          '<div class="article-title">' +
                            '<p>' + val.title + '</p>' +
                          '</div>' +
                          '<div class="article-text">' +
                            '<p>' + val.meta.socialDescription.substring(0,80) + '...&nbsp;<a href="https://blog.xcidic.com/article/' + val.slug + '" target="_blank" class="read-more" rel="noopener noreferrer">READ MORE</a>' +
                            '</p>' +
                          '</div>' +
                        '</a>' +
                      '</div>' +
                      '</li>');
        var articleForMobile = ('<li class="article row list-unstyled" id="article_' + key + '">' +
                            '<a href="https://blog.xcidic.com/article/' + val.slug + '" target="_blank" rel="noopener noreferrer">' +
                            '<div class="col-xs-4">' +
                              '<img class="img-responsive" src="' + val.imageThumbSquare + '">' +
                            '</div>' +
                            '<div class="col-xs-8 article-title">' +
                              '<p>' + val.title +  '</p>' +
                            '</div>' +
                          '</a>' +
                          '</li>');
        itemsForDesktop.push(articleForDesktop);
        itemsForMobile.push(articleForMobile);
      });
      $("<ul/>", {
        "class": "article-desktop",
        html: itemsForDesktop.join('')
      }).appendTo($('#for_desktopp'));
      $("<ul/>", {
        "class": "article-mobile",
        html: itemsForMobile.join('')
      }).appendTo($('#for_mobile'));
    });

    function validate1(){
        if (isEmail($('#email1').val()) && ($('#enquiry1').val().length > 0)) {
          contact_button1.prop("disabled", false);
        }
        else {
          contact_button1.prop("disabled", true);
        }
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    function restApi(endpoint) {
      var restApi = 'https://blog.xcidic.com/' + endpoint;
      return restApi;
    }
 });
})();
